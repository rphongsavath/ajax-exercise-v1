## Project Name: 
AJAX Exercise v1

## Project Decription: 
This application displays a series of  names and images of employees from a mockAPI endpoint source. The loading screen is set for a 2 second increment and will populate the individual cards for reach employee. 

## Deployment
1. Copy the application from the GitLab repository
2. Navigate to the application folder
3. Right click in the folder and select "Git Bash Here"
4. Navigate up one folder to the src folder
5. Use npm install to download the necessary depenencies to run app
6. Type 'npm start' in the Git Bash terminal and application should start local port 3000 in a seperate browser window. If port is already in use, type 'npx kill-port 3000' in the Git Bash terminal. Rerun 'npm start'.
7. Open application folder in Visual Studio to review code. 
8. If edits to the mockAPI endpoint are need, please use the following link: https://mockapi.io/projects/64179a8b7159106502072b3f


## Author: Richy Phongsavath