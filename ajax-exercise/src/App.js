
import './App.css';
import {useState, useEffect} from 'react'
import UserCard from './components/UserCard';
import {Audio} from 'react-loader-spinner';
import LoadingSpinner from './components/LoadingSpinner';

function App() {
 const API_URL = 'https://64179a8b7159106502072b3e.mockapi.io/user'

 const [items, setItems] = useState([]);
 const [fetchError,setFetchError] = useState(null);
 const [loading, setLoading] = useState(true);

useEffect(() =>{
  const fetchItems = async () => {
    try{
      const response = await fetch(API_URL);

      //catches any errors if there is an error after attempting to fetch from API
      if (!response.ok) throw Error('Opps something went wrong')

      const listItems = await response.json();
      
      setItems(listItems);
      setFetchError(null);

    } catch(err){
      
      setFetchError(err.message);

    } finally {
      setLoading(false);
    }
  }
setTimeout(() =>{
 fetchItems();
}, 2000)
 

}, [])


  return (
    <div className="App">
      <h1>Employee Directory</h1> 
      {loading && <LoadingSpinner/>}
      {fetchError }
      <div className = "cardContainer">
        <div className="userDisplay">
          {items.map((user)=>(
            
            <UserCard
              key = {user.id}>
              <img src ={user.image}/>
              <p >{user.name}</p>
              </UserCard>
          
          ))}
        </div>
     </div> 
    </div>
  );
}
 
export default App;
